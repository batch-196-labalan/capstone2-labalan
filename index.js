const express = require("express");
const mongoose = require("mongoose"); //ODM library to let our expressjs api manipulate a nongodb database. Added after installation of mongoose.

const app = express();
const port = process.env.PORT || 4000;
const cors = require('cors');

mongoose.connect("mongodb+srv://admin:admin123@cluster0.5ed30qq.mongodb.net/ECommerceAPI?retryWrites=true&w=majority",
{
	useNewUrlParser: true,
	useUnifiedTopology: true

});

let db = mongoose.connection; 

db.on('error',console.error.bind(console, "MongoDB Connection Error.")); 

db.once('open',()=>console.log("Connected to MongoDB."))




app.use(express.json());  
//middleware
app.use(cors());



const userRoutes = require("./routes/userRoutes");
app.use('/users',userRoutes);

const productRoutes = require("./routes/productRoutes");
app.use('/products',productRoutes);


app.listen(port,()=> console.log('Express API running at port 4000'))
