
const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");


module.exports.registerUser = (req,res)=>{

    const hashedPw = bcrypt.hashSync(req.body.password,10);
    console.log(hashedPw);
    
    let newUser = new User({

        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashedPw,
        mobileNo: req.body.mobileNo
    })

    newUser.save()
    .then(result => res.send(result))
    .catch(error => res.send(error))
    

}


//USER LOGIN

module.exports.loginUser = (req,res) => {

    // console.log(req.body);


User.findOne({email:req.body.email})
.then(foundUser => {

    if(foundUser === null){
        return res.send(false);//({message: "No User Found."})
    } else {
        // console.log(foundUser)


        const isPasswordCorrect = bcrypt.compareSync(req.body.password,foundUser.password);

        // console.log(isPasswordCorrect);


        if(isPasswordCorrect){

            return res.send({accessToken: auth.createAccessToken(foundUser)});

        } else {

            return res.send(false);//({message: "Incorrect Password"});
        }

    }
})


}



module.exports.getUserDetails = (req,res) => {

    // let userId = req.body._id

    // User.find({userId})
    // User.findOne({_id:req.body.id})
    // .then(result => res.send(result))
    // .catch(error => res.send(error))

 User.findById(req.user.id)
    .then(result => res.send(result))
    .catch(error => res.send(error))

}


//Update user to admin
module.exports.updateAdmin = (req,res)=>{


    let update = {

        isAdmin: true
    }

    User.findByIdAndUpdate(req.params.userId,update,{new:true})

    .then(result => res.send(result))
    .catch(error => res.send(error))

}



//ORDER

module.exports.order = async (req,res) => {

    if(req.user.isAdmin){
        return res.send(false);//({message: "Action Forbidden"});
    }

    
    let isUserUpdated = await User.findById(req.user.id).then(user => {

        // console.log(user);


        let newOrder = {

            totalAmount: req.body.totalAmount, //{}
            products: req.body.products
        }

        
        user.orders.push(newOrder)

        return user.save()
        .then(user => true)
        .catch(err => err.message)

    })

    console.log(isUserUpdated);


    if(isUserUpdated !== true){
        return res.send({message: isUserUpdated});
    }

 
    let getOrders = req.body.products.forEach(function(getOrders){


        Product.findById(getOrders.productId).then(product=>{
            let order = {
                userId: req.user.id,
                quantity: getOrders.quantity
            }
            product.orders.push(order);
            return product.save().then(product => true).catch(err => err.message);
        })
     }) 
     
     if(isUserUpdated){
        return res.send(true);//({message:"Order Successful."})
     }
 }      

       


//GET AUTHENTICATED USER ORDERS
module.exports.getUserOrders = (req,res) =>{
    if(req.user.isAdmin){
        return res.send({message: "Action Forbidden"});
    } else {


    User.findById(req.user.id).select("orders")
    .then(result => res.send(result))
    .catch(error => res.send(error))

    }
}



//GET ALL ORDERS (ADMIN)
module.exports.getAllOrders = (req,res) =>{
   
    User.find({isAdmin:false},{orders:1,id:1})
    .then(result => res.send(result))
    .catch(error => res.send(error))

    }


// [SECTION] CHECK IF EMAIL EXISTS

module.exports.checkEmailExists = (req,res) => {

    User.findOne({email: req.body.email})
    .then(result => {

        //console.log(result)

        if(result === null){
            return res.send(false);
        } else {
            return res.send(true)
        }

    })
    .catch(err => res.send(err));
}













