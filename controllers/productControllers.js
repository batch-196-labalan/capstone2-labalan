const Product = require("../models/Product");


//Add product - Admin
module.exports.addProduct = (req,res)=>{

    // console.log(req.body);

let newProduct = new Product({
	
	name: req.body.name,
	description: req.body.description,
	price: req.body.price

  })
// console.log(newCourse);

	newProduct.save()
	.then(result => res.send(result))
	.catch(error => res.send(error))

}


//get all products
module.exports.getAllProducts = (req,res)=>{

    Product.find({isActive:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))
}



//get single product

module.exports.getSingleProduct = (req,res)=>{

    // console.log(req.params) 

    // console.log(req.params.courseId)

    Product.findById(req.params.productId)
    .then(result => res.send(result))
    .catch(error => res.send(error))

}



//UPDATE PRODUCT
module.exports.updateProduct = (req,res)=>{


    let update = {

        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }

    Product.findByIdAndUpdate(req.params.productId,update,{new:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))


}


//ARCHIVE PRODUCT
module.exports.archiveProduct = (req,res)=>{


    let update = {

        isActive: false
    }

    Product.findByIdAndUpdate(req.params.productId,update,{new:true})

    .then(result => res.send(result))
    .catch(error => res.send(error))

}



//ACTIVATE PRODUCT

module.exports.activateProduct = (req,res) =>{

    let update = {
        isActive: true
    }

    Product.findByIdAndUpdate(req.params.productId,update,{new:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))

}


//DISPLAY ALL PRODUCTS PER ORDER (USER ONLY)
module.exports.getOrderDetails = (req,res)=>{


    User.findById(req.user.id,{_id:0,orders:1})
    .then(result => {

        let products = result.orders
        let index = products.map(function(orders) { 
            return orders.id; }).indexOf(req.params.orderId);

        return res.send({id:req.params.orderId,products:products[index].products})
    })
    .catch(error => res.send(error))

}







