
const express = require("express");
const router = express.Router();

const userControllers = require("../controllers/userControllers");

// console.log(userControllers); 

const auth = require("../auth"); 
const { verify,verifyAdmin } = auth;


router.post("/",userControllers.registerUser);

router.post("/login",userControllers.loginUser);

router.get('/getUserDetails',verify,userControllers.getUserDetails);


//update user to admin
router.put("/updateAdmin/:userId",verify,verifyAdmin,userControllers.updateAdmin);


//---

//ORDER
router.post('/order',verify,userControllers.order);

//AUTHENTICATED USER ORDERS
router.get('/getUserOrders',verify,userControllers.getUserOrders)



//GET ALL ORDERS (ADMIN)
router.get('/getAllOrders',verify,verifyAdmin,userControllers.getAllOrders)



// check if email exists
router.post('/checkEmailExists',userControllers.checkEmailExists);



module.exports = router;