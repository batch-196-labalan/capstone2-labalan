const express = require("express");
const router = express.Router();

const productControllers = require("../controllers/productControllers");

const auth = require("../auth"); 
const { verify,verifyAdmin } = auth;


router.post('/',verify,verifyAdmin,productControllers.addProduct);


//get all active products
router.get('/active',productControllers.getAllProducts);


//get single product
router.get('/getSingleProduct/:productId',productControllers.getSingleProduct);


//update product
router.put("/updateProduct/:productId",verify,verifyAdmin,productControllers.updateProduct);


//archive product
router.delete("/archiveProduct/:productId",verify,verifyAdmin,productControllers.archiveProduct);


//activate product
router.put("/activateProduct/:productId",verify,verifyAdmin,productControllers.activateProduct)




module.exports = router;